import {Meteor} from 'meteor/meteor'
import {Template} from 'meteor/templating'
import {TAPi18n} from 'meteor/tap:i18n'

import {ReactiveDict} from 'meteor/reactive-dict'
import Feedback from '../../../api/feedback'

import {Jobs, ActivityNames, StatusNames} from '../../../api/jobs/jobs'

import './grid.html'
import './grid.styl'

Meteor.subscribe('jobs');

Template.grid.onCreated(function gridOnCreated() {
    Session.set('search_query', null);
    Session.set('search_activity', null);
    Session.set('search_status', null);
    Session.set('grid_limit', 200);
    Session.set('selectedRecords', []);
    Session.set('selectInProgress', false);

    window.onscroll = () => {
        let window_top = $(window).scrollTop(),
            anchor = $('.scroll-anchor'),
            head = $('.head'),
            div_top = anchor.offset().top;

        if (window_top > div_top) {
            head.addClass('stick');
            anchor.height(head.outerHeight());
        } else {
            head.removeClass('stick');
            anchor.height(0);
        }
    };
});

Template.grid.helpers({
    'jobs'(){
        let filter = {},
            sorter = {endDate: -1},
            curKey = Session.get('sorter_key') || 'endDate',
            curDir = Session.get('sorter_dir') || -1;

        if (curKey === 'timeLeft') {
            //Сортируем сначала до статусу, потом до дате
            sorter = {status: 1, endDate: curDir}
        } else {
            //Сначала сортируем по ключу, потом по jobID
            sorter = {[curKey]: curDir, jobID: curDir};
        }


        //Поиск по тексту
        let query = Session.get('search_query');
        if (query) {
            query = new RegExp(query.trim().replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&'), 'i');
            filter = {
                $or: [
                    {jobID: query},
                    {productTypeName: query},
                    {installerName: query},
                    {accessList: query},
                ]
            };
        }

        //Фильтр по типу работы
        let search_activity = Session.get('search_activity');
        if (search_activity && search_activity !== '-1') {
            filter.activityID = +search_activity;
        }


        //Фильтр по статусу работы
        let search_status = Session.get('search_status');
        if (search_status && search_status !== '-1') {
            filter.status = +search_status;
        }
        return Jobs.find(filter, {sort: sorter, limit: Session.get('grid_limit')});
    },
    'activityNames'(){
        let result = [];
        for (let key in ActivityNames) {
            result.push({key, value: ActivityNames[key]});
        }
        return result;
    },
    'statusNames'(){
        let result = [];
        for (let key in StatusNames) {
            result.push({key, value: StatusNames[key]});
        }
        return result;
    },
    'activeCount'(){
        return Jobs.find({status: 1}).count()
    },
    'readyCount'(){
        return Jobs.find({status: 3}).count()
    },
    'allowedToUseControls'(){
        return (Session.get('allowedToUseControls'));
    },
    'selectInProgress'(){
        return (Session.get('selectInProgress'));
    },
    'toggleSelectionText'(){
        return (Session.get('selectInProgress') ? TAPi18n.__('grid.cancel') : TAPi18n.__('grid.select'));
    },
    'serverTime'(){
        return moment.utc(TimeSync.serverTime()).format('DD.MM.YYYY HH:mm:ss');
    }
});

Template.grid.events({
    'click .sorters .cell'(e){
        let tgtKey = e.target.dataset.sortKey;
        let curKey = Session.get('sorter_key');
        if (curKey) {
            //Ключ существует, сравниваем с текущим
            if (curKey === tgtKey) {
                //Инвертируем сортировку
                let curDir = Session.get('sorter_dir');
                Session.set('sorter_dir', -curDir);
            } else {
                //Сортируем с новым ключом
                Session.set('sorter_key', tgtKey);
                Session.set('sorter_dir', 1);
            }
        } else {
            Session.set('sorter_key', tgtKey);
            Session.set('sorter_dir', 1);
        }
    },
    'input .filter.search'(e){
        Session.set('search_query', e.target.value);
    },
    'change .filter.activity'(e){
        Session.set('search_activity', e.target.value);
    },
    'change .filter.status'(e){
        Session.set('search_status', e.target.value);
    },
    'click .toggleSelection'(e, inst){
        Session.set('selectInProgress', !Session.get('selectInProgress'));
    },
    'submit form.massUpdater'(e, inst){
        e.preventDefault();
        const form = e.target;
        let chars = _.map(form.list.value.trim().split(','), (char) => {
            return char.trim();
        });
        let requisites = form.reqs.value.trim();
        let feedback = new Feedback();
        Meteor.call(
            'jobs.massUpdate',
            Session.get('selectedRecords'),
            chars,
            requisites,
            (err, res) => {
                if (err) {
                    feedback.show(TAPi18n.__('error.server'), 3000)
                } else {
                    feedback.show(res, 3000);
                }
            });
    }
});

Template.job.onCreated(function jobOnCreated() {
    this.state = new ReactiveDict();
    this.state.set('expanded', false);
    this.state.set('selected', false);
});

Template.job.helpers({
    'boundJobs'(){
        return Jobs.find({boundTo: this.jobID});
    },
    'boundJobsCount'(){
        return Jobs.find({boundTo: this.jobID}).count();
    },
    'isBound'(boundTo){
        return (boundTo && boundTo !== '0');
    },
    'shared'(accessList){
        return accessList.length;
    },
    'date'(date){
        return moment.utc(date).format('DD.MM.YY HH:mm');
    },
    'statusName'(status){
        switch (+status) {
            case 0:
                return TAPi18n.__('status.none');
            case 1:
                return TAPi18n.__('status.active');
            case 2:
                return TAPi18n.__('status.paused');
            case 3:
                return TAPi18n.__('status.ready');
            case 101:
                return TAPi18n.__('status.delivered');
            case 102:
                return TAPi18n.__('status.cancelled');
            case 103:
                return TAPi18n.__('status.reverted');
            default:
                return TAPi18n.__('status.unknown');
        }
    },
    'activityName'(activityID){
        switch (+activityID) {
            case 0:
                return TAPi18n.__('activity.none');
            case 1:
                return TAPi18n.__('activity.manufacturing');
            case 2:
                return TAPi18n.__('activity.researchTech');
            case 3:
                return TAPi18n.__('activity.researchTE');
            case 4:
                return TAPi18n.__('activity.researchME');
            case 5:
                return TAPi18n.__('activity.copying');
            case 6:
                return TAPi18n.__('activity.duplication');
            case 7:
                return TAPi18n.__('activity.reverse');
            case 8:
                return TAPi18n.__('activity.invention');
            default:
                return TAPi18n.__('activity.unknown');
        }
    },
    'edit'(){
        return Template.instance().state.get('edit');
    },
    'editRequisites'(){
        return Template.instance().state.get('editRequisites');
    },
    'left'(date){
        let diff = date - TimeSync.serverTime();
        if (diff > 0)
            return moment.duration(diff).format('M[mo] d[d] hh:mm:ss');
    },
    'color'(status){
        switch (status) {
            case 1: {
                return 'job-in-progress'
            }
            case 3: {
                return 'job-ready'
            }
            case 101: {
                return 'job-done'
            }
        }
    },
    'expanded'(){
        return Template.instance().state.get('expanded');
    },
    'getAccessList'(accessList){
        return accessList.length ? accessList : TAPi18n.__('grid.closed')
    },
    'getRequisites'(requisites){
        requisites = requisites && requisites.trim();
        return requisites? requisites: TAPi18n.__('grid.noRequisites')
    },
    'stringifyAccessList'(accessList){
        let string = '';

        accessList.map((val, i, arr) => {
            i && (string += ', ');
            string += val;
        });
        return string;
    },
    'allowedToUseControls'(){
        return (Session.get('allowedToUseControls'));
    },
    'selectInProgress'(){
        let sip = Session.get('selectInProgress');
        !sip && Template.instance().state.set('selected', false);
    },
    'selected'(){
        return Template.instance().state.get('selected');
    }
});

Template.job.events({
    'click .accessList'(e, inst){
        if (Session.get('allowedToUseControls')) {
            inst.state.set('edit', true);
            Meteor.defer(() => {
                inst.find('input').focus();
            });
        }
    },
    'click .expandable-content .requisites'(e, inst){
        if (Session.get('allowedToUseControls')) {
            inst.state.set('editRequisites', true);
            Meteor.defer(() => {
                inst.find('input').focus();
            });
        }
    },
    'keypress .accessList input'(e, inst){
        if (e.which === 13) {
            let chars = _.map(e.target.value.trim().split(','), (char) => {
                return char.trim();
            });
            let feedback = new Feedback();
            Meteor.call(
                'jobs.updateAccessList',
                inst.data.jobID,
                chars,
                (err, res) => {
                    if (err) {
                        feedback.show(TAPi18n.__('error.server'), 3000)
                    } else {
                        feedback.show(res, 3000);
                    }
                });
            inst.state.set('edit', false);
        }
    },
    'keypress .requisites input'(e, inst){
        if (e.which === 13) {
            let feedback = new Feedback();
            Meteor.call(
                'jobs.updateRequisites',
                inst.data.jobID,
                e.target.value.trim(),
                (err, res) => {
                    if (err) {
                        feedback.show(TAPi18n.__('error.server'), 3000)
                    } else {
                        feedback.show(res, 3000);
                    }
                });
            inst.state.set('editRequisites', false);
        }
    },
    'focusout .accessList'(e, inst){
        inst.state.get('edit') && inst.state.set('edit', false)
    },
    'focusout .requisites'(e, inst){
        inst.state.get('editRequisites') && inst.state.set('editRequisites', false)
    },
    'click .main > .productTypeName'(e, inst){
        !Session.get('selectInProgress') && inst.state.set('expanded', !inst.state.get('expanded'));
    },
    'keypress input.bindTo'(e, inst){
        if (e.which === 13) {
            let targetID = e.target.value.trim(),
                targetJob = Jobs.findOne({jobID: targetID});
            let feedback = new Feedback();
            if (!targetID) {
                feedback.show(TAPi18n.__('message.inputJobId'));
                return;
            } else if (targetID === inst.data.jobID) {
                feedback.show(TAPi18n.__('message.cantBindToOneself'));
                return;
            } else if (!targetJob) {
                feedback.show(TAPi18n.__('message.jobNotExists', {job: targetID}));
                return;
            } else if (targetJob.boundTo && targetJob.boundTo !== '0') {
                feedback.show(TAPi18n.__('message.jobAlreadyBound', {
                    job: targetID,
                    boundTo: Jobs.findOne({jobID: targetID}).boundTo
                }));
                return;
            }
            Meteor.call(
                'jobs.bind',
                inst.data.jobID,
                targetID,
                (err, res) => {
                    if (err) {
                        feedback.show(TAPi18n.__('error.server'), 3000)
                    } else {
                        feedback.show(res, 3000);
                    }
                });
            inst.state.set('edit', false);
        }
    },
    'click .unbind'(e, inst){
        let feedback = new Feedback();
        Meteor.call(
            'jobs.unbind',
            inst.data.jobID,
            (err, res) => {
                if (err) {
                    feedback.show(TAPi18n.__('error.server'), 3000)
                } else {
                    feedback.show(res, 3000);
                }
            });
    },
    'click .record > .row.main'(e, inst) {
        if (Session.get('selectInProgress')) {
            let newState = !inst.state.get('selected');
            inst.state.set('selected', newState);
            let selected = Session.get('selectedRecords');
            newState ? selected.push(inst.data.jobID) : selected.splice(selected.indexOf(inst.data.jobID), 1);
            Session.set('selectedRecords', selected);
        }
    }
});

Template.boundJob.onCreated(function jobOnCreated() {
    this.state = new ReactiveDict();
    this.state.set('expanded', false);
});

Template.boundJob.helpers({
    'statusName'(status){
        switch (+status) {
            case 0:
                return TAPi18n.__('status.none');
            case 1:
                return TAPi18n.__('status.active');
            case 2:
                return TAPi18n.__('status.paused');
            case 3:
                return TAPi18n.__('status.ready');
            case 101:
                return TAPi18n.__('status.delivered');
            case 102:
                return TAPi18n.__('status.cancelled');
            case 103:
                return TAPi18n.__('status.reverted');
            default:
                return TAPi18n.__('status.unknown');
        }
    },
    'activityName'(activityID){
        switch (+activityID) {
            case 0:
                return TAPi18n.__('activity.none');
            case 1:
                return TAPi18n.__('activity.manufacturing');
            case 2:
                return TAPi18n.__('activity.researchTech');
            case 3:
                return TAPi18n.__('activity.researchTE');
            case 4:
                return TAPi18n.__('activity.researchME');
            case 5:
                return TAPi18n.__('activity.copying');
            case 6:
                return TAPi18n.__('activity.duplication');
            case 7:
                return TAPi18n.__('activity.reverse');
            case 8:
                return TAPi18n.__('activity.invention');
            default:
                return TAPi18n.__('activity.unknown');
        }
    },
    'edit'(){
        return Template.instance().state.get('edit');
    },
    'editRequisites'(){
        return Template.instance().state.get('editRequisites');
    },
    'date'(date){
        return moment.utc(date).format('DD.MM.YY HH:mm');
    },
    'left'(date){
        let diff = date - TimeSync.serverTime();
        if (diff > 0)
            return moment.duration(diff).format('M[mo] d[d] hh:mm:ss');
    },
    'color'(status){
        switch (status) {
            case 1: {
                return 'job-in-progress'
            }
            case 3: {
                return 'job-ready'
            }
            case 101: {
                return 'job-done'
            }
        }
    },
    'expanded'(){
        return Template.instance().state.get('expanded');
    },
    'getAccessList'(accessList){
        return accessList.length ? accessList : TAPi18n.__('error.closed')
    },
    'getRequisites'(requisites){
        requisites = requisites && requisites.trim();
        return requisites? requisites: TAPi18n.__('grid.noRequisites')
    },
    'allowedToUseControls'(){
        return (Session.get('allowedToUseControls'));
    }
});

Template.boundJob.events({
    'click .productTypeName'(e, inst){
        inst.state.set('expanded', !inst.state.get('expanded'));
    },
    'click .unbind'(e, inst){
        let feedback = new Feedback();
        Meteor.call(
            'jobs.unbind',
            inst.data.jobID,
            (err, res) => {
                if (err) {
                    feedback.show(TAPi18n.__('error.server'), 3000)
                } else {
                    feedback.show(res, 3000);
                }
            });
    }
});