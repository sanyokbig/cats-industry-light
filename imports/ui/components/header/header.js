import {Meteor} from 'meteor/meteor'
import {Template} from 'meteor/templating'
import {TAPi18n} from 'meteor/tap:i18n'

import {Keys} from '../../../api/keys/keys'

import Feedback from '../../../api/feedback'

import './header.html';
import './header.styl';

Meteor.subscribe('keys');

Template.header.onCreated(function headerOnCreated() {
    this.state = new ReactiveDict();
    this.state.set('key', {});
    this.state.set('showControls', false);
    Session.set('allowedToUseControls', false);
    Meteor.call('user.isAdmin', (err, res) => {
        Session.set('allowedToUseControls', res);
    })
});

Template.header.helpers({
    'keys'(){
        return Keys.find({});
    },
    'editDisabled'(){
        let key = Template.instance().state.get('key') || {};
        return (!key.type || key.type !== 'corp');
    },
    'edit'(){
        let inst = Template.instance();
        return (inst.state.get('edit') && inst.state.get('key').type === 'corp');
    },
    'industrialistsList'(){
        return Template.instance().state.get('key').industrialists.join(', ');
    },
    'showControls'(){
        return (Template.instance().state.get('showControls'));
    },
    'allowedToUseControls'(){
        return (Session.get('allowedToUseControls'));
    },
    'serverResetInProgress'(){
        return Session.get('serverResetInProgress');
    }
});

Template.header.events({
    'click .login'(){
        let inst = Template.instance();
        Meteor.loginWithEve({
            requestPermissions: ['publicData']
        }, (err) => {
            if (err) {
                console.log(err);
            }
            Meteor.call('user.isAdmin', (err, res) => {
                Session.set('allowedToUseControls', res);
            })
        });
    },
    'click .logout'(){
        Meteor.logout();
    },
    'submit .add'(e){
        e.preventDefault();
        let keyID = e.target.keyID.value,
            vCode = e.target.vCode.value;
        let feedback = new Feedback();
        if (!keyID || !vCode) {
            feedback.show(TAPi18n.__('message.fillBothFields'), 4000);
            return;
        }

        Meteor.call('keys.add', keyID, vCode, (err, res) => {
            if (err) {
                feedback.show(TAPi18n.__('error.server'), 3000)
            } else {
                feedback.show(res, 3000);
                e.target.keyID.value = e.target.vCode.value = '';
            }
        });
    },
    'click .editIndustrialists'(e, inst){
        inst.state.set('key', Keys.findOne({keyID: +inst.find('select').value}));
        inst.state.set('edit', +inst.find('.keys>select').value);
    },
    'click .edit .yes'(e, inst){
        let chars = _.map(inst.find('textarea').value.trim().split(/\n|,/), (char) => {
            return char.trim();
        });
        let feedback = new Feedback();
        Meteor.call('keys.updateIndustrialists', inst.state.get('edit'), chars, (err, res) => {
            if (err) {
                feedback.show(TAPi18n.__('error.server'), 3000)
            } else {
                feedback.show(res, 3000);
            }
        });
        inst.state.set('edit', null);
    },
    'click .edit .no'(e, inst){
        inst.state.set('edit', null);
    },
    'click .delete'(e, inst){
        let feedback = new Feedback();
        let selectedKey = inst.find('select').value;
        if (!selectedKey) {
            feedback.show(TAPi18n.__('message.selectKey'), 2000);
            return;
        }
        new Confirmation({
                message: TAPi18n.__('message.removeKey'),
                title: TAPi18n.__('general.confirmation'),
                cancelText: TAPi18n.__('general.cancel'),
                okText: TAPi18n.__('general.yes'),
                success: true, // whether the button should be green or red
                focus: "cancel"
            },
            function (ok) {
                if (!ok)return;
                Meteor.call('keys.remove', selectedKey, (err, res) => {
                    if (err) {
                        feedback.show(TAPi18n.__('error.server'), 3000)
                    } else {
                        feedback.show(res, 3000);
                        inst.state.set('key', {});
                    }
                })
            }
        );
    },
    'change .keys>select'(e, inst){
        inst.state.set('key', Keys.findOne({keyID: +e.target.value}));
    },
    'click .showControls'(e, inst) {
        inst.state.set('showControls', true);
    },
    'click .hideControls'(e, inst) {
        inst.state.set('showControls', false);
    },
    'change .languageSelector'(e, inst){
        TAPi18n.setLanguage(e.target.value);
    }
});
