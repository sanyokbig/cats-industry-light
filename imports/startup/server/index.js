import {Meteor} from 'meteor/meteor'

import Watcher from '../../api/watcher'
import UserDataManager from '../../api/userData/userDataManager';

import './eve-config.js';

import '../../api/jobs/server/methods.js';
import '../../api/jobs/server/publications.js';

import '../../api/keys/server/methods.js';
import '../../api/keys/server/publications.js';

import '../../api/userData/server/methods.js';

Meteor.startup(() => {
    Global = {
        dbBusy: false
    };

    new UserDataManager().onCreateUser();
    new Watcher().watch();
});