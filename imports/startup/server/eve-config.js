import {Meteor} from 'meteor/meteor'

ServiceConfiguration.configurations.upsert(
    { service: 'eve' },
    {
        $set: {
            clientId: Meteor.settings.eveapp.clientId,
            loginStyle: 'popup',
            secret: Meteor.settings.eveapp.secret
        }
    }
);