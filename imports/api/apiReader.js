import {HTTP} from 'meteor/http';
import {Meteor} from 'meteor/meteor'

export default class ApiReader {
    parseError(err) {
        let code = err.code || err.response.statusCode,
            text,
            tryAgain;

        switch (code) {
            case 'ETIMEDOUT': {
                text = 'Timeout';
                tryAgain = true;
                break;
            }
            case 503: {
                text = 'API Server is unavailable';
                tryAgain = true;
                break;
            }
            case 400:
            case 403: {
                text = xml2js.parseStringSync(err.response.content).eveapi.error[0]._;
                break;
            }
            default: {
                console.log('Cannot parse an error: ');
                console.log(err);
                text = err.response ? xml2js.parseStringSync(err.response.content) : err;
            }
        }

        return {code, text, tryAgain}
    }

    getKeyInfo(keyID, vCode, attempt = 0) {
        return new Promise((resolve, reject) => {
            HTTP.get('https://api.eveonline.com/account/APIKeyInfo.xml.aspx', {
                params: {keyID, vCode},
                timeout: Meteor.settings.api.timeoutTime
            }, (err, res) => {
                if (err) {
                    let {code, text, tryAgain} = this.parseError(err);
                    if (tryAgain) {
                        if (attempt < Meteor.settings.api.timeoutAttempts) {
                            attempt++;
                            console.log('getKeyInfo, ', text, ', trying again, ', attempt);
                            this.getKeyInfo(keyID, vCode, attempt).then(res => resolve(res)).catch(err => reject(err));
                        } else {
                            console.log('getKeyInfo, ', text, ', timeout limit reached');
                            reject(text);
                        }
                    } else {
                        console.log('getKeyInfo, ', text, ' reject');
                        reject(text);
                    }
                } else {
                    let parsed = xml2js.parseStringSync(res.content);
                    resolve(parsed);
                }
            });
        });
    }

    async getJobs(key, isHistory, attempt = 0) {
        return new Promise((resolve, reject) => {
            let params = {
                keyID: key.keyID,
                vCode: key.vCode,
                charID: key.charID
            };
            HTTP.call('get', 'https://api.eveonline.com/' + key.type + '/IndustryJobs' + (isHistory ? 'History' : '') + '.xml.aspx', {
                    params,
                    timeout: Meteor.settings.api.timeoutTime
                }, (err, res) => {
                    if (err) {
                        let {code, text, tryAgain} = this.parseError(err);
                        if (tryAgain) {
                            if (attempt < Meteor.settings.api.timeoutAttempts) {
                                attempt++;
                                console.log('getJobs', (isHistory ? 'History' : ''), ', ', text, ', trying again, ', attempt);
                                this.getJobs(key, isHistory, attempt).then(res => resolve(res)).catch(err => reject(err));
                            } else {
                                console.log('getJobs', (isHistory ? 'History' : ''), ', ', text, ', timeout limit reached');
                                reject(text);
                            }
                        } else {
                            console.log('getJobs', (isHistory ? 'History' : ''), text, ' reject');
                            reject(text);
                        }
                    } else {
                        xml2js.parseString(res.content, (err, res) => {
                            let jobs = res.eveapi.result[0].rowset[0].row || [],
                                result = [];

                            console.log('Got ' + jobs.length + ' jobs');
                            if (jobs) {
                                //Тянем работы не старше 30 дней
                                let minDate = new Date((new Date) - 86400000 * Meteor.settings.jobs.filterAge);
                                for (let job of jobs) {
                                    if ((new Date(job.$.startDate) - minDate > 0) && (!key.industrialists.length || (key.industrialists.includes(job.$.installerName)))) {
                                        result.push(job.$);
                                    }
                                }
                            }
                            console.log('Got ' + result.length + ' jobs after filtering');
                            resolve(result);
                        });
                    }
                }
            );
        });
    }
}