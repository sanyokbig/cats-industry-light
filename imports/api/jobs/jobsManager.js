import {Jobs} from './jobs.js'
import {Keys} from '../keys/keys'

import ApiReader from '../apiReader'
import KeysManager from '../keys/keysManager'

export default class JobsManager {
    add(job) {
        if ((+job.status === 1) && (moment.utc() - moment.utc(job.endDate) > 0)) {
            job.status = 3;
        }
        try {
            job.startDate = moment.utc(job.startDate).format();
            job.endDate = moment.utc(job.endDate).format();
            job.completedDate = moment.utc(job.completedDate).format();

            if (Jobs.findOne({jobID: job.jobID})) {
                Jobs.update({jobID: job.jobID}, {$set: job});
            } else {
                job.accessList = [];
                job.boundTo = "0";
                Jobs.insert(job);
            }
        } catch (e) {
            console.log('Failed to update or insert job: ' + e);
        }
    }

    addList(list) {
        for (let job of list) {
            try {
                this.add(job);
            } catch (e) {
                console.log(e);
            }
        }
    }

    async updateJobs(isHistory) {
        const apiReader = new ApiReader(),
            keysManager = new KeysManager(),
            jobsManager = new JobsManager();

        for (let key of Keys.find({}).fetch()) {
            //Проверять ключ, если все ок, работаем
            key = await keysManager.update(key);
            if (key) {
                try {
                    let jobs = await apiReader.getJobs(key, isHistory);
                    if (jobs.length) {
                        //Проходим по подтянутым работам и обновляем данные
                        jobsManager.addList(jobs);
                    }
                } catch (err) {
                    console.log('updateJobs, ', err);
                }
            }
        }
    }

    updateStatuses() {
        let i = 0;
        for (let job of Jobs.find({status: 1}).fetch()) {
            if (moment.utc() - moment.utc(job.endDate) > 0) {
                Jobs.update({_id: job._id}, {$set: {status: 3}});
                i++
            }
        }
        if (i) {
            console.log('Corrected ' + i + ' jobs');
        }
    }

    updateAccessList(jobID, accessList) {
        try {
            Jobs.update({jobID}, {
                $set: {
                    accessList
                }
            });
        } catch (e) {
            console.log(e);
            return 'Ошибка сервера'
        }
        return 'Список доступа обновлен'
    }

    updateRequisites(jobID, requisites) {
        try {
            Jobs.update({jobID}, {
                $set: {
                    requisites
                }
            });
        } catch (e) {
            console.log(e);
            return 'Ошибка сервера'
        }
        return 'Реквизиты обновлены'
    }



    massUpdate(jobIDList, accessList, requisites) {
        for(let jobID of jobIDList) {
            let setter={
                accessList,
                requisites: requisites || null
            };
            try {
                Jobs.update({jobID}, {
                    $set: setter
                });
            } catch (e) {
                console.log(e);
                return 'Ошибка сервера'
            }
        }
        return 'Обновление успешно'
    }

    clean(days) {
        try {
            let removed = Jobs.remove({
                completedDate: {
                    $lt: new Date((new Date) - days * 86400000)
                },
                status: {
                    $gt: 100
                }
            });
            console.log('Cleaned ' + removed + ' jobs');
        } catch (e) {
            console.log(e)
        }
    }

    async bind(bindableID, targetID) {
        let targetJob = Jobs.findOne({jobID: targetID});
        if (!targetID) {
            return('Введите ID работы');
        } else if (targetID === bindableID) {
            return('Нельзя привязаться к себе');
        } else if (!targetJob) {
            return('Работа ' + targetID + ' не существует');
        } else if (targetJob.boundTo && targetJob.boundTo !== '0') {
            return('Работа ' + targetID + ' привязана к другой работе ' + targetJob.boundTo);
        }
        try {
            Jobs.update({jobID: bindableID}, {$set: {boundTo: targetID}});
            console.log('Job ' + bindableID + ' bound to ' + targetID);
            return 'Привязка успешна'
        } catch (e) {
            console.log(e);
            return 'Ошибка сервера'
        }
    }

    async unbind(jobID) {
        try {
            Jobs.update({jobID}, {$set: {boundTo: '0'}});
            console.log('Job ' + jobID + ' unbound');
            return 'Отвязка успешна'
        } catch (e) {
            console.log(e);
            return 'Ошибка сервера'
        }
    }
}
