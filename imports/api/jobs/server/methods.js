import {Meteor} from 'meteor/meteor'
import JobsManager from '../jobsManager'
import AccessManager from '../../accessManager'

Meteor.methods({
    'jobs.updateAccessList'(jobID, accessList){
        const accessManager = new AccessManager();
        if (accessManager.isAdmin(Meteor.user())) {
            return new JobsManager().updateAccessList(jobID, accessList);
        }
        return 'Недостаточно прав'
    },
    'jobs.updateRequisites'(jobID, requisites){
        const accessManager = new AccessManager();
        if (accessManager.isAdmin(Meteor.user())) {
            return new JobsManager().updateRequisites(jobID, requisites);
        }
        return 'Недостаточно прав'
    },
    'jobs.massUpdate'(jobIDList, accessList, requisites){
        const accessManager = new AccessManager();
        if (accessManager.isAdmin(Meteor.user())) {
            return new JobsManager().massUpdate(jobIDList, accessList, requisites);
        }
        return 'Недостаточно прав'
    },
    'jobs.bind'(bindableID, targetID){
        const accessManager = new AccessManager();
        if (accessManager.isAdmin(Meteor.user())) {
            return new JobsManager().bind(bindableID, targetID);
        }
        return 'Недостаточно прав'
    },
    'jobs.unbind'(jobID){
        const accessManager = new AccessManager();
        if (accessManager.isAdmin(Meteor.user())) {
            return new JobsManager().unbind(jobID);
        }
        return 'Недостаточно прав'
    }
});