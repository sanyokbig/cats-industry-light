import {Meteor} from 'meteor/meteor';
import {Jobs} from '../jobs';

Meteor.publish('jobs', function () {
    if (this.userId) {
        let roles = Meteor.users.findOne(this.userId).roles || [];
        if (roles.includes('admin')) {
            return Jobs.find({});
        }
        //Generate list of chained jobs to show
        let accessedJobs = Jobs.find({
            accessList: Meteor.users.findOne(this.userId).services.eve.character.name,
        }).fetch();
        let jobIDs = _.map(accessedJobs, (job) => {
            return job.jobID;
        });
        return Jobs.find({
            $or: [
                {accessList: RegExp(Meteor.users.findOne(this.userId).services.eve.character.name, 'i')},
                {boundTo: {$in: jobIDs}}
            ]
        }, {
            fields: {
                jobID: 1,
                boundTo: 1,
                activityID: 1,
                productTypeName: 1,
                runs: 1,
                status: 1,
                solarSystemName: 1,
                startDate: 1,
                endDate: 1,
                completedDate: 1,
                accessList: 1
            }
        });
    }
});