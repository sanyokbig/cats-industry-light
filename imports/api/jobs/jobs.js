import {Mongo} from 'meteor/mongo';

import SimpleSchema from 'simpl-schema';

export const Jobs = new Mongo.Collection('jobs');

const Schemas = Schemas || {};

Schemas.Job = new SimpleSchema({
    jobID: {
        type: String,
        label: 'Job ID'
    },
    boundTo: {
        type: String,
        label: 'Bound To',
        optional: true
    },
    installerName: {
        type: String,
        label: 'Installer Name'
    },
    activityID: {
        type: Number,
        label: 'Activity ID'
    },
    productTypeName: {
        type: String,
        label: 'Installer Name'
    },
    runs: {
        type: Number,
        label: 'Runs'
    },
    status: {
        type: Number,
        label: 'Status'
    },
    solarSystemName: {
        type: String,
        label: 'Solar System'
    },
    startDate: {
        type: Date,
        label: 'Start Date'
    },
    endDate: {
        type: Date,
        label: 'End date'
    },
    completedDate: {
        type: Date,
        label: 'Completed Date'
    },
    accessList: {
        type: Array,
        label: 'Access List',
        optional: true
    },
    'accessList.$':{
        type: String
    },
    requisites: {
        type: String,
        label: 'Requisites',
        optional: true
    }
});


Jobs.attachSchema(Schemas.Job);