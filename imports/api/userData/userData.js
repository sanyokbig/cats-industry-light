import {Mongo} from 'meteor/mongo';

import SimpleSchema from 'simpl-schema';

export const UserData = new Mongo.Collection('userData');

const Schemas = Schemas || {};

Schemas.userData = new SimpleSchema({
    characterID: {
        type: Number,
        label: 'Character ID'
    },
    characterName: {
        type: String,
        label: 'Character Name'
    },
    roles: {
        type: Array,
        label: 'Roles'
    },
    'roles.$':{
        type: String
    }
});

UserData.attachSchema(Schemas.userData);
