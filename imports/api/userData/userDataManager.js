import {Accounts} from 'meteor/accounts-base'

import {UserData} from './userData';

export default class UserDataManager {
    onCreateUser() {
        Accounts.onCreateUser((options, user) => {
            let roles = this.getRoles(user.services.eve.character);
            return Object.assign({},
                options,
                user,
                roles);
        })
    }

    getRoles(character) {
        let userData = UserData.findOne({characterName: character.name});
        if (!userData) {
            UserData.insert({
                characterName: character.name,
                characterID: character.id,
                roles: []
            });
            return {roles: []}
        }
        return {roles: userData.roles}
    }
}