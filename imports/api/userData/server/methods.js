import {Meteor} from 'meteor/meteor';

Meteor.methods({
    'user.isAdmin'(){
        let user = Meteor.user();
        return (user && user.roles || []).includes('admin');
    }
});