export default class Bit {
    mask(num, mask) {
        return ((num & mask) === mask);
    }
}