export default class AccessManager {
    isAdmin(user) {
        return (user.roles || []).includes('admin');
    }
}