import {Meteor} from 'meteor/meteor';

import KeysManager from '../keysManager'
import AccessManager from '../../accessManager'

Meteor.methods({
    'keys.add'(keyID, vCode){
        const accessManager = new AccessManager();
        if(accessManager.isAdmin(Meteor.user())){
            return new KeysManager().add(keyID, vCode);
        }
        return 'Недостаточно прав'
    },
    'keys.updateIndustrialists'(keyID, industrialists){
        const accessManager = new AccessManager();
        if(accessManager.isAdmin(Meteor.user())) {
            return new KeysManager().updateIndustrialists(keyID, industrialists) ? 'Производственники обновлены' : 'Ошибка обновления производственников'
        }
        return 'Недостаточно прав'
    },
    'keys.remove'(keyID){
        const accessManager = new AccessManager();
        if(accessManager.isAdmin(Meteor.user())) {
            return new KeysManager().remove(keyID) ? 'Ключ удален' : 'Ошибка удаления ключа';
        }
        return 'Недостаточно прав'
    }
});