import {Meteor} from 'meteor/meteor';
import {Keys} from '../keys';

Meteor.publish('keys', function(){
    if(this.userId) {
        let roles = (Meteor.users.findOne(this.userId).roles) || [];
        if(roles.includes('admin')) {
            return Keys.find({},{fields: {keyID:1, ownerName:1, type:1, industrialists: 1}});
        }
        return false;
    }
});