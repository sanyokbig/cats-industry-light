import ApiReader from '../apiReader'
import Bit from '../bit'

import {Keys} from './keys'

let Future = Npm.require('fibers/future');

export default class KeysManager {
    async add(keyID, vCode) {
        let apiReader = new ApiReader(),
            bit = new Bit(),
            response;
        try {
            response = await apiReader.getKeyInfo(keyID, vCode);
        } catch (err) {
            return (err);
        }

        response = response.eveapi.result[0].key[0];
        let keyInfo = response.$,
            charInfo = response.rowset[0].row[0].$;
        //Актуальность маски
        if (bit.mask(keyInfo.accessMask, 128)) {
            if (Keys.findOne({keyID: +keyID})) {
                return ('Ключ уже существует');
            } else {
                try {
                    Keys.insert({
                        keyID: +keyID,
                        vCode,
                        owner: +charInfo.characterID,
                        ownerName: charInfo.characterName,
                        industrialists: [],
                        type: keyInfo.type === 'Character' ? 'char' : 'corp'
                    });
                    return ('Ключ добавлен');
                } catch (e) {
                    return ('Ошибка добавления. Попробуйте еще раз.');
                }
            }

        }
        return ('Неверная маска');
    }

    async update(key) {
        let apiReader = new ApiReader(),
            bit = new Bit(),
            response;
        try {
            response = await apiReader.getKeyInfo(key.keyID, key.vCode);
        } catch (err) {
            return (false);
        }
        response = response.eveapi.result[0].key[0];
        let keyInfo = response.$;
        //Актуальность маски
        if (bit.mask(keyInfo.accessMask, 128)) {
            let newType = (keyInfo.type === 'Character') ? 'char' : 'corp';

            if (newType !== key.type) {
                key.type = newType;
                try {
                    Keys.update(key._id, {$set: key});
                } catch (e) {
                    return (false);
                }
            }
            return (key);
        }
        //Маска не подходит, пропускаем
        return (false);
    }

    updateIndustrialists(keyID, industrialists) {
        try {
            Keys.update({keyID: +keyID}, {
                $set: {
                    industrialists
                }
            });
            return true
        } catch (e) {
            console.log(e);
            return false
        }
    }

    remove(keyID) {
        try {
            Keys.remove({keyID: +keyID});
            return true;
        } catch (err) {
            console.log(err);
            return false
        }
    }
}