import {Meteor} from 'meteor/meteor'

import JobsManager from './jobs/jobsManager'
import StringParser from './stringParser'

export default class Watcher {
    watch() {
        let jobsManager = new JobsManager,
            stringParser = new StringParser();
        const settings = Meteor.settings;
        jobsManager.updateJobs();
        SyncedCron.add({
            name: 'Update Jobs',
            schedule: function(parser){
                return parser.cron(stringParser.parse(settings.schedule.updateJobs))
            },
            job: function () {
                jobsManager.updateJobs()
            }
        });

        SyncedCron.add({
            name: 'Update Jobs History',
            schedule: function(parser){
                return parser.cron(stringParser.parse(settings.schedule.updateJobsHistory))
            },
            job: function () {
                jobsManager.updateJobs(true)
            }
        });

        SyncedCron.add({
            name: 'Update Statuses',
            schedule: function(parser){
                return parser.cron(stringParser.parse(settings.schedule.updateStatuses))
            },
            job: function () {
                jobsManager.updateStatuses()
            }
        });

        SyncedCron.add({
            name: 'Clean Jobs',
            schedule: function(parser){
                return parser.cron(stringParser.parse(settings.schedule.clean))
            },
            job: function () {
                jobsManager.clean(settings.jobs.cleanAge)
            },
        });

        SyncedCron.start();
    };
}